program daemon;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  System.IOUtils,
  Posix.stdLib,
  Posix.Unistd,
  Posix.SysTypes,
  Posix.SysStat,
  Posix.Signal,
  Posix.linuxhelper in 'src\posix.linuxhelper.pas',
  Posix.Utils in 'src\posix.Utils.pas',
  Posix.Syslog in 'src\posix.Syslog.pas';

var
  input, Param, Path, root: String;
  running: boolean;

const
  EXIT_FAILURE = 1;
  EXIT_SUCCESS = 0;
  PROCNAME = 'visertoDS';

procedure HandleSignals(SigNum: Integer); cdecl;
begin
  case SigNum of
    SIGTERM:
      begin
        running := False;
      end;
    SIGHUP:
      begin
        running := true;
      end;
  end;
end;

begin
  try
    { TODO -oUser -cConsole Main : Code hier einf�gen }
    openlog(nil, LOG_PID or LOG_NDELAY, LOG_DAEMON);
    if fork() <> 0 then
    begin
      exit;
    end;
    root := ExtractFilePath(ParamStr(0));
    Path := TPath.Combine(root, Format('%s &', [PROCNAME]));
    Signal(SIGCHLD, TSignalHandler(SIG_IGN));
    Signal(SIGHUP, HandleSignals);
    Signal(SIGTERM, HandleSignals);
    running := true;
    while running do
    begin
      // Cloud wird in schleife immer wieder neu gestartet...
      Syslog(LOG_NOTICE, Format('%s cloud daemon -> start %s cloud.',
        [PROCNAME, PROCNAME]));
      TUtils.StartCloud(Path);
      Syslog(LOG_NOTICE, Format('%s cloud daemon -> cloud was closed!',
        [PROCNAME]));
      Sleep(200);
    end;
    ExitCode := EXIT_SUCCESS;
    Syslog(LOG_NOTICE, Format('%s cloud daemon -> daemon process exit.',
      [PROCNAME]));

  except
    on E: Exception do
    begin
      Syslog(LOG_ERR, Format('%s cloud daemon -> Error: %s',
        [PROCNAME, E.Message]));
      writeLn(E.ClassName, ': ', E.Message);
      ExitCode := EXIT_FAILURE;
    end;
  end;

end.
