unit posix.Utils;

interface

uses
  System.SysUtils,
  System.IOUtils,
  System.classes,
  posix.stdLib,
  posix.Unistd,
  posix.SysTypes,
  posix.SysStat,
  posix.Signal;

type
  TUtils = class
    class function APICall(const Address: String; User: String): Boolean;
    class function StartCloud(const Path: String): Boolean;
  end;

const
  SudoPWD: String = 'E1jcm!CZqxQd';

implementation

uses
  posix.linuxhelper;

{ TUtils }

class function TUtils.APICall(const Address: String; User: String): Boolean;
var
  R: TStringList;
  S: String;
begin
  try
    Result := false;
    R := TLinuxUtils.RunCommandLine(Format('curl --user %s -i -X GET %s',
      [User.QuotedString, Address.QuotedString]));
    for S in R do
    begin
      Result := (S <> '');
      if Result then
        break;
    end;
  finally
    R.Clear;
    FreeAndNil(R);
  end;
end;

class function TUtils.StartCloud(const Path: String): Boolean;
var
  Sudo: String;
begin
  Sudo := Format('echo "%s" | sudo -S ', [SudoPWD]);
  TLinuxUtils.RunCommandLine(Sudo + Path);
end;

end.
