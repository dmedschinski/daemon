Build an Daemon Script on Linux:

Step1: Create executable

change to /etc/init.d
cmd: sudo nano myname

// use the script Template in ../templates

change or replace vars with our captions

{
NAME="servicename"
PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
APPDIR="/../../xyz"
APPBIN="./processname"
APPARGS=""
USER="root"
GROUP="sudo"
}

Optional:
To be kill other process or dependend process you can use the command killall in function "stop"
show the outcommented lines!

stop() {
  printf "Stopping '$NAME'... "
  [ -z `cat /var/run/$NAME.pid 2>/dev/null` ] || \
  while test -d /proc/$(cat /var/run/$NAME.pid); do
    killtree $(cat /var/run/$NAME.pid) 15
    sleep 0.5
  done 
  [ -z `cat /var/run/$NAME.pid 2>/dev/null` ] || rm /var/run/$NAME.pid
  # killall -9 daemon
  # killall -9 sentiboCS
  printf "done\n"
}


finalize:
 make the file excutable: sudo chmod +x /etc/init.d/myname
 register as service: sudo update-rc.d myname defaults
